package hw_3_1;

import org.jetbrains.annotations.Contract;

import java.math.BigDecimal;
import java.math.MathContext;

public class Card {

    private final String cardHolderName;
    private double balance;

    Card(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    Card(String cardHolderName, double balance) {
        this.cardHolderName = cardHolderName;
        this.balance = balance;
    }

    /**
     * This method takes a @param add and if add above zero
     * add value will add to card balance
     */
    void addBalance(double add) {
        if (add > 0) {
            BigDecimal actualBalance = BigDecimal.valueOf(balance);
            BigDecimal updateBalance = actualBalance.add(BigDecimal.valueOf(add));
            balance = updateBalance.doubleValue();
        }

    }

    /**
     * This method takes a @param reduce and if reduce above zero
     * reduce value will withdraw from card balance
     */
    void withdraw(double reduce) {
        if (reduce < balance && reduce > 0) {
            BigDecimal actualBalance = BigDecimal.valueOf(balance);
            BigDecimal updateBalance = actualBalance.subtract(BigDecimal.valueOf(reduce));
            balance = updateBalance.doubleValue();
        }
    }


    String getCardHolderName() {
        return cardHolderName;
    }

    String getBalances() {
        return String.valueOf(balance);
    }

    void setBalance(double balance) {
        this.balance = balance;
    }

}
