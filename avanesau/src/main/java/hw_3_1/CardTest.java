package hw_3_1;

import org.junit.Assert;
import org.junit.Test;


public class CardTest {

    @Test
    public void testCreateCardByNameAddBalance() {
        double adder = 3.28;
        Card card1 = new Card("David Avanesau");
        card1.addBalance(adder);
        Assert.assertEquals("3.28", card1.getBalances());
    }

    @Test
    public void testCreateCardAddBalance() {
        double adder = 3.28;
        Card card1 = new Card("David Avanesau", 27.92);
        card1.addBalance(adder);
        Assert.assertEquals("31.2", card1.getBalances());
    }

    @Test
    public void testCreateCardByNameWithdrawNotError() {
        double reducer = 3.28;
        Card card1 = new Card("David Avanesau");
        card1.withdraw(reducer);
        Assert.assertEquals("0.0", card1.getBalances());
    }

    @Test
    public void testCreateCardWithdraw() {
        double reducer = 3.28;
        Card card1 = new Card("David Avanesau", 100);
        card1.withdraw(reducer);
        Assert.assertEquals("96.72", card1.getBalances());
    }

    @Test
    public void testCreateCardWithdrawLessZero() {
        double reducer = -1;
        Card card1 = new Card("David Avanesau", 122.15);
        card1.withdraw(reducer);
        Assert.assertEquals("122.15", card1.getBalances());
    }

    @Test
    public void testCreateCardAddLessZero() {
        double adder = -1;
        Card card1 = new Card("David Avanesau", 122.15);
        card1.addBalance(adder);
        Assert.assertEquals("122.15", card1.getBalances());
    }

}
