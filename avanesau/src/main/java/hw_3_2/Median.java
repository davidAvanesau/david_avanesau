package hw_3_2;

import java.util.Arrays;

public class Median {

    /**
     * This method takes @param array of int and find median value of array and
     *
     * @return this value
     */
    static float median(int[] array) {
        Arrays.sort(array);
        if (array.length % 2 == 0) {
            float answer;
            answer = (array[array.length / 2] + array[array.length / 2 - 1]);
            answer /= 2;
            return answer;
        } else {
            return array[array.length / 2];
        }
    }

    /**
     * This method takes @param array of double values and find median value of array and
     *
     * @return this value
     */
    static double median(double[] array) {
        Arrays.sort(array);
        if (array.length % 2 == 0) {
            double answer;
            answer = (array[array.length / 2] + array[array.length / 2 - 1]);
            answer /= 2;
            return answer;
        } else {
            return array[array.length / 2];
        }
    }
}
