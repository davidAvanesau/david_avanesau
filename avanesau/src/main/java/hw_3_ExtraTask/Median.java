package hw_3_ExtraTask;

import java.util.Arrays;

public class Median {

    static float median(int[] array) {
        float answer = 0;
        int countResults = 0;
        int[] helpArray = new int[array.length + 2];
        int middleElementNumber = 0;
        if (array.length % 2 == 0) {
            countResults = getCountResults(array, helpArray);
            if (countResults == 2) {
                answer = helpArray[array.length] + helpArray[array.length + 1];
                answer /= 2;
            } else if (countResults == 1) {
                middleElementNumber = getMiddleElementNumber(array, helpArray);
                answer = findElementByDistance(array, helpArray, middleElementNumber);
                return answer;
            } else if (countResults == 0) {
                answer = findElementByDistance(array, helpArray);
                return answer;
            }
        } else {
            answer = getMiddleElement(array, helpArray);
        }
        return answer;
    }


    static double median(double[] array) {
        double answer = 0;
        int countResults = 0;
        double[] helpArray = new double[array.length + 2];
        int middleElementNumber = 0;
        if (array.length % 2 == 0) {
            countResults = getCountResults(array, helpArray);
            if (countResults == 2) {
                answer = helpArray[array.length] + helpArray[array.length + 1];
                answer /= 2;
            } else if (countResults == 1) {
                middleElementNumber = getMiddleElementNumber(array, helpArray);
                answer = findElementByDistance(array, helpArray, middleElementNumber);
                return answer;
            } else if (countResults == 0) {
                answer = findElementByDistance(array, helpArray);
                return answer;
            }
        } else {
            answer = getMiddleElement(array, helpArray);
        }
        return answer;
    }

    /**
     * This method takes arrays, @return 0 if there are no middle elements in first array, 1 if only one middle element,
     * 2 if there ara two middle elements
     *
     * @param array
     * @param helpArray
     */
    private static int getCountResults(int[] array, int[] helpArray) {
        int countResults = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (array[i] <= array[j]) {
                    helpArray[i]++;
                }
            }
            if (helpArray[i] == (array.length / 2)) {
                if (countResults == 0) {
                    helpArray[array.length] = array[i];
                } else {
                    helpArray[array.length + 1] = array[i];
                }
                countResults++;
            } else if (helpArray[i] == (array.length / 2) + 1) {
                if (countResults == 0) {
                    helpArray[array.length] = array[i];
                } else {
                    helpArray[array.length + 1] = array[i];
                }
                countResults++;
            }
        }
        return countResults;
    }

    /**
     * This method takes arrays, @return position of one middle element.
     * It need to know is this middle element before, or after middle value.
     * This method is been used only when array is even and only one middle element was found
     *
     * @param array
     * @param helpArray
     */
    private static int getMiddleElementNumber(int[] array, int[] helpArray) {
        int middleElementNumber = 0;
        int countResults = 0;
        for (int i = 0; i < array.length; i++) {
            if (helpArray[i] == (array.length / 2)) {
                if (countResults == 0) {
                    helpArray[array.length] = array[i];
                } else {
                    helpArray[array.length + 1] = array[i];
                }
                countResults++;
                middleElementNumber = 1;
            } else if (helpArray[i] == (array.length / 2) + 1) {
                if (countResults == 0) {
                    helpArray[array.length] = array[i];
                } else {
                    helpArray[array.length + 1] = array[i];
                }
                countResults++;
                middleElementNumber = 2;
            }
        }
        return middleElementNumber;
    }

    /**
     * This method @return element that have minimal length from pseudo-element that could be median
     *
     * @param array
     * @param helpArray
     */
    private static float findElementByDistance(int[] array, int[] helpArray) {
        helpArray = new int[array.length];
        int minDistance = helpArray.length;
        int minDistanceIndex = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (array[i] < array[j]) {
                    helpArray[i]++;
                }
            }
        }
        for (int i = 0; i < helpArray.length; i++) {
            int distance = Math.abs((array.length / 2) - helpArray[i]);
            if (minDistance > distance) {
                minDistance = distance;
                minDistanceIndex = i;
            }
        }
        return array[minDistanceIndex];
    }

    /**
     * This method @return element that have minimal length from real element that before or after median value
     *
     * @param array
     * @param helpArray
     */
    private static float findElementByDistance(int[] array, int[] helpArray, int middleElementNumber) {
        int minDistance = Integer.MAX_VALUE;
        int minDistanceIndex = 0;
        for (int i = 0; i < helpArray.length - 2; i++) {
            if (middleElementNumber == 1) {
                int distance = Math.abs((helpArray[array.length]) - array[i]);
                if (minDistance > distance && helpArray[array.length] > array[i]) {
                    minDistance = distance;
                    minDistanceIndex = i;
                }
            } else {
                int distance = Math.abs((helpArray[array.length + 1]) - array[i]);
                if (minDistance > distance && helpArray[array.length + 1] < array[i]) {
                    minDistance = distance;
                    minDistanceIndex = i;
                }
            }
        }
        float answer;
        answer = (helpArray[array.length] + array[minDistanceIndex]) / 2;
        return answer;
    }

    /**
     * This method @return element from array that have count of array elements before this element equal
     * to count of elements that after
     *
     * @param array
     * @param helpArray
     * @return
     */
    private static float getMiddleElement(int[] array, int[] helpArray) {
        float answer = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (array[i] >= array[j]) {
                    helpArray[i]++;
                }
            }
            if (helpArray[i] == (array.length / 2) + 1) {
                answer = array[i];
                return answer;
            }
        }
        return answer;
    }

    /**
     * This method takes arrays, @return 0 if there are no middle elements in first array, 1 if only one middle element,
     * 2 if there ara two middle elements
     *
     * @param array
     * @param helpArray
     */
    private static int getCountResults(double[] array, double[] helpArray) {
        int countResults = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (array[i] <= array[j]) {
                    helpArray[i]++;
                }
            }
            if (helpArray[i] == (array.length / 2)) {
                if (countResults == 0) {
                    helpArray[array.length] = array[i];
                } else {
                    helpArray[array.length + 1] = array[i];
                }
                countResults++;
            } else if (helpArray[i] == (array.length / 2) + 1) {
                if (countResults == 0) {
                    helpArray[array.length] = array[i];
                } else {
                    helpArray[array.length + 1] = array[i];
                }
                countResults++;
            }
        }
        return countResults;
    }

    /**
     * This method takes arrays, @return position of one middle element.
     * It need to know is this middle element before, or after middle value.
     * This method is been used only when array is even and only one middle element was found
     *
     * @param array
     * @param helpArray
     */
    private static int getMiddleElementNumber(double[] array, double[] helpArray) {
        int middleElementNumber = 0;
        int countResults = 0;
        for (int i = 0; i < array.length; i++) {
            if (helpArray[i] == (array.length / 2)) {
                if (countResults == 0) {
                    helpArray[array.length] = array[i];
                } else {
                    helpArray[array.length + 1] = array[i];
                }
                countResults++;
                middleElementNumber = 1;
            } else if (helpArray[i] == (array.length / 2) + 1) {
                if (countResults == 0) {
                    helpArray[array.length] = array[i];
                } else {
                    helpArray[array.length + 1] = array[i];
                }
                countResults++;
                middleElementNumber = 2;
            }
        }
        return middleElementNumber;
    }

    /**
     * This method @return element that have minimal length from pseudo-element that could be median
     *
     * @param array
     * @param helpArray
     */
    private static double findElementByDistance(double[] array, double[] helpArray) {
        helpArray = new double[array.length];
        double minDistance = helpArray.length;
        int minDistanceIndex = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (array[i] < array[j]) {
                    helpArray[i]++;
                }
            }
        }
        for (int i = 0; i < helpArray.length; i++) {
            double distance = Math.abs((array.length / 2) - helpArray[i]);
            if (minDistance > distance) {
                minDistance = distance;
                minDistanceIndex = i;
            }
        }
        return array[minDistanceIndex];
    }

    /**
     * This method @return element that have minimal length from real element that before or after median value
     *
     * @param array
     * @param helpArray
     */
    private static double findElementByDistance(double[] array, double[] helpArray, int middleElementNumber) {
        double minDistance = Integer.MAX_VALUE;
        int minDistanceIndex = 0;
        for (int i = 0; i < helpArray.length - 2; i++) {
            if (middleElementNumber == 1) {
                double distance = Math.abs((helpArray[array.length]) - array[i]);
                if (minDistance > distance && helpArray[array.length] > array[i]) {
                    minDistance = distance;
                    minDistanceIndex = i;
                }
            } else {
                double distance = Math.abs((helpArray[array.length + 1]) - array[i]);
                if (minDistance > distance && helpArray[array.length + 1] < array[i]) {
                    minDistance = distance;
                    minDistanceIndex = i;
                }
            }
        }
        double answer;
        answer = (helpArray[array.length] + array[minDistanceIndex]) / 2;
        return answer;
    }

    /**
     * This method @return element from array that have count of array elements before this element equal
     * to count of elements that after
     *
     * @param array
     * @param helpArray
     * @return
     */
    private static double getMiddleElement(double[] array, double[] helpArray) {
        double answer = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (array[i] >= array[j]) {
                    helpArray[i]++;
                }
            }
            if (helpArray[i] == (array.length / 2) + 1) {
                answer = array[i];
                return answer;
            }
        }
        return answer;
    }
}
