package hw_4_1;

public class Atm {

    /**
     * This method takes @param card then if card is not null method add @param cash
     * to balance of this card
     */
    static void add(Card card, double cash) {
        if (card != null) {
            card.addBalance(cash);
        }
    }

    /**
     * This method takes @param card then if card is not null method withdraw @param cash
     * from balance of this card
     */
    static void withdraw(Card card, double cash) {
        if (card != null) {
            card.withdraw(cash);
        }
    }

    /**
     * This method @return balance of @param card
     */
    static String getBalance(Card card) {
        return card.getBalance();
    }
}
