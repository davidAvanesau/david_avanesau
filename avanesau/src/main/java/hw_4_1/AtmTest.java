package hw_4_1;

import org.junit.Assert;
import org.junit.Test;

public class AtmTest {

    @Test
    public void testDebitWithdrawBalanceLessZero() {
        double reducer = 3.28;
        Card card1 = new DebitCard("David Avanesau");
        Atm.withdraw(card1, reducer);
        Assert.assertEquals("0.0", card1.getBalance());
    }

    @Test
    public void testDebitWithdraw() {
        double reducer = 3.28;
        Card card1 = new DebitCard("David Avanesau", 13.28);
        Atm.withdraw(card1, reducer);
        Assert.assertEquals("10.0", card1.getBalance());
    }

    @Test
    public void testDebitAdd() {
        double adder = 3.28;
        Card card1 = new DebitCard("David Avanesau");
        Atm.add(card1, adder);
        Assert.assertEquals("3.28", card1.getBalance());
    }

    @Test
    public void testCreditWithdrawBalanceLessZero() {
        double reducer = 3.28;
        Card card1 = new CreditCard("David Avanesau", 2);
        Atm.withdraw(card1, reducer);
        Assert.assertEquals("-1.28", card1.getBalance());
    }

    @Test
    public void testCreditAddLessZero() {
        double adder = -3.28;
        Card card1 = new CreditCard("David Avanesau", 10);
        Atm.add(card1, adder);
        Assert.assertEquals("10.0", card1.getBalance());
    }

    @Test
    public void testNullCard() {
        double adder = 20;
        Card card1 = new CreditCard("David Avanesau", 10);
        Atm.add(null, adder);
        Assert.assertEquals("10.0", card1.getBalance());
    }

    @Test
    public void testGetBalance() {
        Card card1 = new CreditCard("David Avanesau", 10);
        Assert.assertEquals("10.0", Atm.getBalance(card1));
    }
}
