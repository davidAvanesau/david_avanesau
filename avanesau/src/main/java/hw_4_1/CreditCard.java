package hw_4_1;

public class CreditCard extends Card{

    public CreditCard(String cardHolderName) {
        super(cardHolderName);
    }

    public CreditCard(String cardHolderName, double balance) {
        super(cardHolderName, balance);
    }
}
