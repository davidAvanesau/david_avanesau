package hw_4_1;

public class DebitCard extends Card {
    DebitCard(String cardHolderName) {
        super(cardHolderName);
    }

    DebitCard(String cardHolderName, double balance) {
        super(cardHolderName, balance);
    }

    /**
     * This method  withdraw @param reduce only if reduce less than balance
     */
    @Override
    void withdraw(double reduce) {
        double balance = Double.parseDouble(super.getBalance());
        if (reduce < balance) {
            super.withdraw(reduce);
        }
    }
}
