package hw_4_2;


public class SelectionSort implements Sorter {

    /**
     * This method sort @param array using the insertion method
     */
    public void sort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int j;
            int insertedElement = array[i];
            j = i;
            while (j > 0 && array[j - 1] > insertedElement) {
                array[j] = array[j - 1];
                j--;
            }
            array[j] = insertedElement;
        }
    }
}
