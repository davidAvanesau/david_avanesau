package hw_4_2;

public class SortingContext {

    private Sorter sorter;

    public SortingContext(Sorter sorter) {
        this.sorter = sorter;
    }

    public void execute(int[] array) {
        if (sorter != null) {
            sorter.sort(array);
        }
    }
}
