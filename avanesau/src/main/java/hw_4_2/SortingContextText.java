package hw_4_2;

import org.junit.Assert;
import org.junit.Test;

public class SortingContextText {


    int[] array = new int[]{5, 8, 7, 5, 6, 3, 19, 20, 10, 1};
    int[] sortArray = new int[]{1, 3, 5, 5, 6, 7, 8, 10, 19, 20};

    @Test
    public void testSelectionSort() {
        SortingContext sortingContext = new SortingContext(new SelectionSort());
        sortingContext.execute(array);
        Assert.assertArrayEquals(sortArray, array);
    }

    @Test
    public void testBubbleSort() {
        SortingContext sortingContext = new SortingContext(new BubbleSort());
        sortingContext.execute(array);
        Assert.assertArrayEquals(sortArray, array);
    }

    @Test
    public void testNullSort() {
        SortingContext sortingContext = new SortingContext(null);
        sortingContext.execute(array);
        Assert.assertArrayEquals(array, array);
    }
}
