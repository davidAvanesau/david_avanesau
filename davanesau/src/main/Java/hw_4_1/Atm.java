package hw_4_1;

public class Atm {

    private Card card;

    public Atm(Card card) {
        this.card = card;
    }

    /**
     * This method takes @param card then if card is not null method add @param cash
     * to balance of this card
     */
    public void add(double cash) {
        if (card != null) {
            card.addBalance(cash);
        }
    }

    /**
     * This method takes @param card then if card is not null method withdraw @param cash
     * from balance of this card
     */
    public void withdraw(double cash) {
        if (card != null) {
            card.withdraw(cash);
        }
    }

    /**
     * This method @return balance of @param card
     */
    public double getBalance() {
        return card.getBalance();
    }
}
