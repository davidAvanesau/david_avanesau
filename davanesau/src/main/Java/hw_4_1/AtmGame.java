package hw_4_1;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class AtmGame {

    public static void main(String[] args) throws InterruptedException {
        Card card = new Card("David Avanesau", 500);
        final Atm moneyProducer = new Atm(card);
        final Atm moneyConsumer = new Atm(card);
        final ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        Runnable shut = new Runnable() {
            @Override
            public void run() {
                if (moneyConsumer.getBalance() < 0 || moneyConsumer.getBalance() > 1000) {
                    service.shutdownNow();
                    System.out.println("Final balance is " + moneyConsumer.getBalance());
                }
            }
        };
        Runnable withdraw = new Runnable() {
            @Override
            public void run() {
                moneyConsumer.withdraw(7);
                System.out.println("Withdraw 7$");
                System.out.println("Current balance is " + moneyConsumer.getBalance());
            }
        };
        Runnable add = new Runnable() {
            @Override
            public void run() {
                moneyProducer.add(5);
                System.out.println("Add 5$");
                System.out.println("Current balance is " + moneyConsumer.getBalance());
            }
        };

        service.scheduleAtFixedRate(shut, 0, 500, TimeUnit.MILLISECONDS);
        service.scheduleAtFixedRate(add, 0, 1, TimeUnit.SECONDS);
        service.scheduleAtFixedRate(withdraw, 0, 10, TimeUnit.SECONDS);
        service.scheduleAtFixedRate(add, 0, 1, TimeUnit.SECONDS);
        service.scheduleAtFixedRate(withdraw, 0, 7, TimeUnit.SECONDS);
        service.scheduleAtFixedRate(add, 0, 1, TimeUnit.SECONDS);
        service.scheduleAtFixedRate(withdraw, 0, 5, TimeUnit.SECONDS);
        service.scheduleAtFixedRate(add, 0, 1, TimeUnit.SECONDS);
        service.scheduleAtFixedRate(withdraw, 0, 7, TimeUnit.SECONDS);
        service.scheduleAtFixedRate(add, 0, 1, TimeUnit.SECONDS);
        service.scheduleAtFixedRate(withdraw, 0, 5, TimeUnit.SECONDS);
    }
}
