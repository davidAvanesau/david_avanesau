package hw_5;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileSystem {
    static private List<FileSystemObject> fileSystemObjects = new ArrayList<>();

    /**
     * this method add to list of objects of file system
     * only if path is valid
     *
     * @param path
     */
    public void addObject(String path) {
        String[] subStr;
        String separator = "/";
        subStr = path.split(separator);

        for (int i = 0; i < subStr.length - 1; i++) {
            System.out.println(subStr[i]);
        }

        String name = subStr[subStr.length - 1];
        if (FileSystemValidation.isValid(subStr)) {
            String onlyPath = getPath(subStr);
            if (FileSystemValidation.isUnique(onlyPath, name, fileSystemObjects))
                fileSystemObjects.add(new FileSystemObject(onlyPath, name, FileSystemValidation.isFolder(name)));
        } else {
            System.out.println("Sorry, path is not valid");
        }


    }

    /**
     * this method @return full path to folder or file
     *
     * @param subStr
     * @return
     */
    private static String getPath(String[] subStr) {
        StringBuilder onlyPath = new StringBuilder("/");
        for (int i = 0; i < subStr.length - 1; i++) {
            onlyPath.append(subStr[i]).append("/");
        }
        return onlyPath.toString();
    }

    public static List<FileSystemObject> getFileSystemObjects() {
        return fileSystemObjects;
    }
}
