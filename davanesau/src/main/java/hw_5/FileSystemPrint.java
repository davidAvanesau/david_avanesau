package hw_5;

import java.util.List;

public class FileSystemPrint {
    private static int tabulation = 0;

    /**
     * this method print only root folder and calls print folder method
     *
     * @param fileSystemObjects
     */
    public FileSystemPrint(List<FileSystemObject> fileSystemObjects) {
        System.out.println("root/");
        printFolder("/", fileSystemObjects);

    }

    /**
     * this method print all folders and files in folders
     * dut if the folder contains another folder
     * this method will be called repeatedly during execution,
     * passing the path and name of the found folder
     *
     * @param path
     * @param fileSystemObjects
     */
    private static void printFolder(String path, List<FileSystemObject> fileSystemObjects) {
        tabulation += 2;
        boolean isFolder;
        String name;
        String currentPath;
        for (int i = 0; i < fileSystemObjects.size(); i++) {
            currentPath = fileSystemObjects.get(i).getPath();
            if (currentPath.equals(path)) {
                isFolder = fileSystemObjects.get(i).getFolder();
                name = fileSystemObjects.get(i).getName();
                if (isFolder) {
                    currentPath += name += "/";
                    printTabulation(tabulation);
                    System.out.println(name);
                    printFolder(currentPath, fileSystemObjects);
                } else {
                    printTabulation(tabulation);
                    System.out.println(name);
                }
            }
        }
        tabulation -= 2;
    }

    private static void printTabulation(int tabulation) {
        for (int i = 0; i < tabulation; i++) {
            System.out.print(" ");
        }
    }


}
