package hw_5;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileSystemValidation {

    /**
     * this method @return true only if path contain only folders
     * and last obj in path folder or file
     * file must have "." and 3 letters after "."
     *
     * @param subStr
     */
    public static boolean isValid(String[] subStr) {
        Pattern pattern = Pattern.compile("[A-z0-9]{1,}");
        Matcher matcher;
        for (int i = 0; i < subStr.length - 1; i++) {
            matcher = pattern.matcher(subStr[i]);
            if (!matcher.matches()) {
                return false;
            }
        }
        boolean isFolder = true;
        pattern = Pattern.compile("^[A-z0-9]{1,}$");
        matcher = pattern.matcher(subStr[subStr.length - 1]);
        if (!matcher.matches()) {
            isFolder = false;
        }
        pattern = Pattern.compile("^[A-z0-9]{1,}\\.[a-z]{3,3}$");
        matcher = pattern.matcher(subStr[subStr.length - 1]);
        return matcher.matches() || isFolder;
    }

    public static boolean isFolder(String name) {
        Pattern pattern = Pattern.compile("^[A-z0-9]{1,}\\.[a-z]{3,3}$");
        Matcher matcher = pattern.matcher(name);
        if (matcher.matches()) {
            return false;
        }
        return true;
    }

    public static boolean isPrint(String input) {
        return input.equals("print");
    }

    /**
     * this method @return false if this path already entered by user
     *
     * @param onlyPath
     * @param name
     * @param fileSystemObjects
     */
    public static boolean isUnique(String onlyPath, String name, List<FileSystemObject> fileSystemObjects) {
        String currentName;
        String currentPath;
        for (FileSystemObject fileSystemObject : fileSystemObjects) {
            currentName = fileSystemObject.getName();
            currentPath = fileSystemObject.getPath();
            if (currentName.equals(name) && currentPath.equals(onlyPath)) {
                System.out.println("Sorry, this path is not unique");
                return false;
            }
        }
        return true;
    }
}
