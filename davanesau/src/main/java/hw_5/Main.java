package hw_5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;

public class Main {

    /**
     * this method doing until user enter "print"
     * all what user enter will processed if fileSystem
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        System.out.println("Hello, enter the path in the following format: \n" + "folder/..../folder or file.txt");
        System.out.println("The file extension has only 3 characters");
        System.out.println("Enter \"print\" to finish");
        String input;
        FileSystem fileSystem = new FileSystem();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            input = reader.readLine();
            if (FileSystemValidation.isPrint((input))) {
                FileSystemPrint fileSystemPrint = new FileSystemPrint(FileSystem.getFileSystemObjects());
                break;
            }
            fileSystem.addObject(input);

        }


    }
}
