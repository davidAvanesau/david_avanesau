package hw_6;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        String string = "I heard that you're settled down\n" +
                "That you found a girl and you're married now\n" +
                "I heard that your dreams came true\n" +
                "Guess she gave you things, I didn't give to you\n" +
                "Old friend, why are you so shy?\n" +
                "Ain't like you to hold back or hide from the light\n" +
                "I hate to turn up out of the blue, uninvited\n" +
                "But I couldn't stay away, I couldn't fight it\n" +
                "I had hoped you'd see my face\n" +
                "And that you'd be reminded that for me, it isn't over\n" +
                "Never mind, I'll find someone like you\n" +
                "I wish nothing but the best for you, too\n" +
                "\"Don't forget me, \" I beg\n" +
                "\"I'll remember, \" you said\n" +
                "Sometimes it lasts in love, but sometimes it hurts instead\n" +
                "Sometimes it lasts in love, but sometimes it hurts instead";
        List<StringObject> stringObjectList = StringGrouping.getListOfWords(string);
        StringPrint.printAll(stringObjectList);
    }
}
