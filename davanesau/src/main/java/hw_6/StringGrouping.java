package hw_6;

import javafx.beans.binding.StringBinding;

import java.util.ArrayList;
import java.util.List;

public class StringGrouping {

    /**
     * this method takes string variable,
     * calls method which separate this variable on single words
     * and then add every word to list,
     * if this word occurs several times
     * this method add count to this word.
     *
     * @param str
     * @return list this words and counts
     */
    public static List<StringObject> getListOfWords(String str) {
        List<StringObject> wordsList = new ArrayList<StringObject>();
        String[] wordsArray = StringSeparator.getWordsArray(str);
        for (String s : wordsArray) {
            char letter = s.toCharArray()[0];
            boolean letterIsExist = false;
            for (StringObject stringObject : wordsList) {
                if (stringObject.getLetter() == letter) {
                    boolean wordIsExist = false;
                    for (int k = 0; k < stringObject.getWords().size(); k++) {
                        if (stringObject.getWords().get(k).getWord().equals(s)) {
                            int count = stringObject.getWords().get(k).getCount();
                            count++;
                            stringObject.getWords().get(k).setCount(count);
                            wordIsExist = true;
                            break;
                        }
                    }
                    if (!wordIsExist) {
                        stringObject.getWords().add(new StringWord(s));
                    }
                    letterIsExist = true;
                }
            }
            if (!letterIsExist) {
                wordsList.add(new StringObject(letter, s));
            }
        }
        return wordsList;
    }
}
