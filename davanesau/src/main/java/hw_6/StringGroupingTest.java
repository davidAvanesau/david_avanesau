package hw_6;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class StringGroupingTest {

    @Test
    public void testGetListOfWords() {
        String str = "I, love:Java";
        List<StringObject> stringObjectList = new ArrayList<>();
        stringObjectList.add(new StringObject('I', "I"));
        stringObjectList.add(new StringObject('L', "LOVE"));
        stringObjectList.add(new StringObject('J', "JAVA"));
        for (int i = 0; i < stringObjectList.size(); i++) {
            Assert.assertEquals(StringGrouping.getListOfWords(str).get(i).getLetter(), stringObjectList.get(i).getLetter());
            for (int j = 0; j < stringObjectList.get(i).getWords().size(); j++) {
                Assert.assertEquals(StringGrouping.getListOfWords(str).get(i).getWords().get(j).getWord(), stringObjectList.get(i).getWords().get(j).getWord());
                Assert.assertEquals(StringGrouping.getListOfWords(str).get(i).getWords().get(j).getCount(), stringObjectList.get(i).getWords().get(j).getCount());
            }
        }
    }
}
