package hw_6;

import java.util.ArrayList;
import java.util.List;

public class StringObject {

    private char letter;
    private List<StringWord> words;

    public StringObject(char letter, String word) {
        this.letter = letter;
        words = new ArrayList<StringWord>();
        words.add(new StringWord(word));
    }

    public char getLetter() {
        return letter;
    }

    public List<StringWord> getWords() {
        return words;
    }

}