package hw_6;

import java.util.List;

public class StringPrint {

    /**
     * this method print out to console all obj of list with string objects
     * in the following format
     * char "letter":
     *               String "word": int "count"
     * example:
     * A:
     *  a: 4
     *  at: 2
     * etc
     */
    public static void printAll(List<StringObject> list) {
        for (int code = 65; code <= 90; code++) {
            for (StringObject stringObject : list) {
                if (code == (int) stringObject.getLetter()) {
                    System.out.println(stringObject.getLetter() + ":");
                    for (int j = 0; j < stringObject.getWords().size(); j++) {
                        System.out.print("  ");
                        System.out.println(stringObject.getWords().get(j).toString());
                    }
                }
            }
        }
    }
}
