package hw_6;

public class StringSeparator {

    /**
     * this method takes string @param str and
     *
     * @return string variable where replace all punctuation marks except for the apostrophe to " "
     */
    private static String deletePunctuation(String str) {
        return str.replaceAll("[^A-z']{1,}", " ").toUpperCase();
    }

    /**
     * this method @return String array where each element is word
     */
    public static String[] getWordsArray(String str) {
        str = deletePunctuation(str);
        String[] strArray;
        String separator = " ";
        strArray = str.split(separator);
        return strArray;
    }
}
