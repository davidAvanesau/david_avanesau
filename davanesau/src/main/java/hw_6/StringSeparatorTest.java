package hw_6;

import org.junit.Assert;
import org.junit.Test;

public class StringSeparatorTest {

    @Test
    public void testGetWordsArray() {
        String str = "I love Java";
        String[] strArray = new String[3];
        strArray[0] = "I";
        strArray[1] = "LOVE";
        strArray[2] = "JAVA";
        Assert.assertArrayEquals(StringSeparator.getWordsArray(str), strArray);
    }

    @Test
    public void testGetWordsArrayNullString() {
        String str = new String();
        String[] strArray = new String[1];
        strArray[0] = "";
        Assert.assertArrayEquals(StringSeparator.getWordsArray(str), strArray);
    }

}
