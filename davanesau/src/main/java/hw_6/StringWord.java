package hw_6;

public class StringWord {

    private String word;
    private int count;

    public StringWord(String word) {
        this.word = word;
        this.count = 1;
    }

    @Override
    public String toString() {
        return word.toLowerCase() + ": " + count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getWord() {
        return word;
    }

    public int getCount() {
        return count;
    }
}
