package hw_7;

public class IntegerValidator implements Validator<Integer> {

    /**
     * this method throw exception if value not in [1,10]
     *
     * @param value
     * @throws ValidationFailedException
     */
    @Override
    public void validate(Integer value) throws ValidationFailedException {
        if (value < 1 || value > 10) {
            throw new ValidationFailedException();
        }
    }
}
