package hw_7;

public class IntegerValidatorFactory extends ValidatorFactory{
    @Override
    public <T> Validator createValidator() {
        return new IntegerValidator();
    }
}
