package hw_7;

public class StringValidator implements Validator<String> {

    /**
     * this method throw exception if value start without upper symbol
     *
     * @param value
     * @throws ValidationFailedException
     */
    @Override
    public void validate(String value) throws ValidationFailedException {
        if (value == null || !value.matches("^[A-Z].*")) {
            throw new ValidationFailedException();
        }
    }
}
