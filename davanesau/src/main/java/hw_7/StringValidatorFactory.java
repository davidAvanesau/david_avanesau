package hw_7;

public class StringValidatorFactory extends ValidatorFactory {
    @Override
    public <T> Validator createValidator() {
        return new StringValidator();
    }
}
