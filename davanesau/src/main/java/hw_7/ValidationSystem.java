package hw_7;

import java.util.HashMap;
import java.util.Map;

public class ValidationSystem {

    private static final Map<String, ValidatorFactory> VALIDATOR_MAP = initMap();

    /**
     * this method validate T value
     * if value is not valid method throw exception
     *
     * @param value
     * @param <T>
     * @throws ValidationFailedException
     */
    public static <T> void validate(T value) throws ValidationFailedException {
        ValidatorFactory validatorFactory = createValidatorByValue(value);
        validatorFactory.createValidator().validate(value);
    }

    /**
     * this method return child of ValidatorFactory thad will validate value
     *
     * @param value
     * @param <T>
     * @return
     * @throws ValidationFailedException
     */
    private static <T> ValidatorFactory createValidatorByValue(T value) throws ValidationFailedException {
        if (value == null) {
            throw new ValidationFailedException();
        }
        return VALIDATOR_MAP.get(value.getClass().getName());
    }

    /**
     * this method initializing map that contain name of type of variables
     * and new class instance for validate value
     *
     * @return
     */
    private static Map<String, ValidatorFactory> initMap() {
        Map<String, ValidatorFactory> validatorMap = new HashMap<>();
        validatorMap.put("java.lang.String", new StringValidatorFactory());
        validatorMap.put("java.lang.Integer", new IntegerValidatorFactory());
        return validatorMap;
    }

}