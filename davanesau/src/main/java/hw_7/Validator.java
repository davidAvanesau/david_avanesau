package hw_7;

public interface Validator<T> {

    void validate(T value) throws ValidationFailedException;
}
