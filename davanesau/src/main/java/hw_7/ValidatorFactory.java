package hw_7;

abstract public class ValidatorFactory {

    abstract public <T> Validator createValidator();
}
