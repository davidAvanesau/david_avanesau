package hw_8_1;

import java.util.ArrayList;

public class LimitedList<E> extends ArrayList<E> {

    private int arraySize;
    private final int DEFAULT_SIZE = 5;
    private Object[] array;
    private int currentSize;
    private final int maxSize = 10;

    public LimitedList() {
        arraySize = DEFAULT_SIZE;
        array = new Object[DEFAULT_SIZE];
        currentSize = 0;
    }

    public LimitedList(int size) {
        arraySize = size;
        array = new Object[size];
        currentSize = 0;
    }


    public int size() {
        return this.currentSize;
    }

    /**
     * add new obj to the end of list
     *
     * @param e
     * @return
     */
    public boolean add(E e) {
        if (currentSize == maxSize) {
            return false;
        } else {
            if (currentSize == arraySize) {
                arrayIncrease();
            }
            array[currentSize] = e;
            currentSize++;
        }
        return true;
    }

    /**
     * increase array if size less than max and size less then need to dave new obj
     */
    private void arrayIncrease() {
        Object[] oldArray = array;
        arraySize *= 1.5;
        if (arraySize > maxSize) {
            arraySize = maxSize;
        }
        array = new Object[arraySize];
        System.arraycopy(oldArray, 0, array, 0, oldArray.length);
    }

    private E getElement(Object obj) {
        return (E) obj;
    }

    public E get(int index) {
        return getElement(array[index]);
    }

    /**
     * remove element by index
     * then array will self copy
     * last element will delete
     *
     * @param index
     * @return
     */
    public E remove(int index) {
        E value = getElement(array[index]);
        if (array.length - 1 - index >= 0) {
            System.arraycopy(array, index + 1, array, index, array.length - 1 - index);
        }
        array[array.length - 1] = null;
        currentSize--;
        return (E) value;
    }

    /**
     * set element[index] new obj
     * jbj that was in this cell delete
     *
     * @param index
     * @param e
     * @return
     */
    public E set(int index, E e) {
        Object value = array[index];
        array[index] = e;
        return getElement(value);
    }

    /**
     * set null to array
     */
    public void clear() {
        for (int i = 0; i < array.length - 1; i++) {
            array[i] = null;
        }
        currentSize = 0;
    }

    public boolean isEmpty() {
        return currentSize == 0;
    }

}
