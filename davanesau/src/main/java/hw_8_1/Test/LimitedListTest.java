package hw_8_1.Test;

import hw_8_1.LimitedList;
import org.junit.Test;

import java.util.List;

public class LimitedListTest {

    @Test
    public void test() {
        List<String> list = new LimitedList<String>();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        list.add("7");
        list.add("8");
        list.add("9");
        list.add("10");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        System.out.println();
        list.set(5, "22222");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        System.out.println();
        list.remove(9);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        System.out.println();
        System.out.println(list.add("10"));
        System.out.println(list.size());
        System.out.println();
        System.out.println(list.add("11"));
        System.out.println();
        System.out.println(list.size());
        System.out.println(list.isEmpty());
        list.clear();
        System.out.println(list.isEmpty());
        System.out.println(list.size());

    }
}
