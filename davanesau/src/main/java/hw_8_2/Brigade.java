package hw_8_2;

import java.util.List;

public class Brigade implements Comparable<Brigade> {

    private List<Worker> workers;
    private final String brigadeName;

    public Brigade(List<Worker> workers, String brigadeName) {
        this.workers = workers;
        this.brigadeName = brigadeName;
    }

    public String getBrigadeName() {
        return brigadeName;
    }

    public List<Worker> getWorkers() {
        return workers;
    }

    public void deleteWorker(Worker worker){
        workers.remove(worker);
    }

    public void setWorkers(List<Worker> workers) {
        this.workers = workers;
    }

    public int getCost() {
        int cost = 0;
        for (Worker worker : workers) {
            cost += worker.getSalary();
        }
        return cost;
    }

    @Override
    public int compareTo(Brigade brigade) {
        return this.getCost() - brigade.getCost();
    }
}
