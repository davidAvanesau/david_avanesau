package hw_8_2;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Tender {

    private Map<Skill, Integer> orderWorkersMap;
    private Map<Skill, Integer> orderWorkersMapCopy;
    private List<Brigade> brigades;
    private Brigade winner;

    public Tender(Map<Skill, Integer> orderWorkersMap, List<Brigade> brigades) {
        this.orderWorkersMap = orderWorkersMap;
        this.brigades = brigades;
        orderWorkersMapCopy = new HashMap<>(orderWorkersMap);
    }

    /**
     * this method return only one brigade which fit to customer
     * if several brigades are fit method return brigade which cost is less
     * if their costs are the same method return first occurrence
     *
     * @throws NonAvailabilityBrigade
     */
    public Brigade goTender() throws NonAvailabilityBrigade {
        boolean breakLoop = false;
        for (Brigade brigade : brigades) {
            for (Worker worker : brigade.getWorkers()) {
                boolean newSkill;
                for (Skill skill : worker.getSkills()) {
                    newSkill = true;
                    for (int i = 0; i < orderWorkersMapCopy.size(); i++) {
                        if (orderWorkersMapCopy.get(skill) > 0 && newSkill) {
                            orderWorkersMapCopy.put(skill, orderWorkersMapCopy.get(skill) - 1);
                            newSkill = false;
                        }
                        if (orderWorkersMapCopy.get(skill) == 0) {
                            orderWorkersMapCopy.remove(skill);
                            breakLoop = true;
                        }
                        if (orderWorkersMapCopy.isEmpty()) {
                            if (winner == null || brigade.compareTo(winner) < 0) {
                                winner = brigade;
                            }
                            breakLoop = true;
                            break;
                        }
                    }
                }
                if (breakLoop) {
                    breakLoop = false;
                    break;
                }
            }
            orderWorkersMapCopy = new HashMap<>(orderWorkersMap);
        }
        if (winner == null) {
            throw new NonAvailabilityBrigade();
        } else {
            return winner;
        }
    }

}
