package hw_8_2;

import java.util.List;

public class Worker {

    private List<Skill> skills;
    private int salary;

    public Worker(List<Skill> skills, int salary) {
        this.skills = skills;
        this.salary = salary;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void addSkills(Skill skill) {
        this.skills.add(skill);
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
