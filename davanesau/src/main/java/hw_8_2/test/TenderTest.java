package hw_8_2.test;

import hw_8_2.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TenderTest {

    @Test
    public void testGoTender() throws NonAvailabilityBrigade {
        Map<Skill, Integer> order = new HashMap<>();
        order.put(Skill.SKILL1, 1);
        order.put(Skill.SKILL2, 1);
        List<Worker> brigade1 = new ArrayList<>();
        List<Worker> brigade2 = new ArrayList<>();
        List<Skill> skills1 = new ArrayList<Skill>();
        skills1.add(Skill.SKILL1);
        skills1.add(Skill.SKILL2);
        brigade1.add(new Worker(skills1, 200));
        brigade1.add(new Worker(skills1, 200));
        brigade1.add(new Worker(skills1, 200));
        Brigade brigade_1 = new Brigade(brigade1, "brigade1");
        brigade2.add(new Worker(skills1, 200));
        brigade2.add(new Worker(skills1, 200));
        Brigade brigade_2 = new Brigade(brigade2, "brigade2");
        List<Brigade> brigades = new ArrayList<>();
        brigades.add(brigade_1);
        brigades.add(brigade_2);
        Tender tender = new Tender(order, brigades);
        Brigade winner = tender.goTender();
        Assert.assertEquals(winner, brigade_2);
    }

    @Test(expected = NonAvailabilityBrigade.class)
    public void testGoTenderNullPointer() throws NonAvailabilityBrigade {
        Map<Skill, Integer> order = new HashMap<>();
        order.put(Skill.SKILL1, 3);
        order.put(Skill.SKILL2, 4);
        List<Worker> brigade1 = new ArrayList<>();
        List<Worker> brigade2 = new ArrayList<>();
        List<Skill> skills1 = new ArrayList<Skill>();
        skills1.add(Skill.SKILL1);
        skills1.add(Skill.SKILL2);
        brigade1.add(new Worker(skills1, 200));
        brigade1.add(new Worker(skills1, 200));
        Brigade brigade_1 = new Brigade(brigade1, "brigade1");
        brigade2.add(new Worker(skills1, 200));
        Brigade brigade_2 = new Brigade(brigade2, "brigade2");
        List<Brigade> brigades = new ArrayList<>();
        brigades.add(brigade_1);
        brigades.add(brigade_2);
        Tender tender = new Tender(order, brigades);
        Brigade winner = tender.goTender();

    }
}
