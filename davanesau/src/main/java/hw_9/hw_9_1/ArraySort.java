package hw_9.hw_9_1;

import java.util.Comparator;

public class ArraySort implements Comparator<Integer> {

    private Integer[] array;

    public ArraySort(Integer[] array) {
        this.array = array;
        if (array == null) {
            this.array = new Integer[0];
        }
    }

    public Integer[] getArray() {
        return array;
    }

    /**
     * this method sort array use compare
     */
    public void sort() {
        boolean isSorted = false;
        int buf;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (compare(array[i], array[i + 1]) > 0) {
                    isSorted = false;

                    buf = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = buf;

                }
            }
        }
    }


    /**
     * comparison by amount of numerals
     *
     * @param o1
     * @param o2
     * @return
     */
    @Override
    public int compare(Integer o1, Integer o2) {
        Integer answer1 = 0;
        while (o1 > 0) {
            answer1 += o1 % 10;
            o1 /= 10;
        }
        int answer2 = 0;
        while (o2 > 0) {
            answer2 += o2 % 10;
            o2 /= 10;
        }
        return answer1.compareTo(answer2);


    }
}
