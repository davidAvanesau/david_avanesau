package hw_9.hw_9_1.Test;

import hw_9.hw_9_1.ArraySort;
import org.junit.Assert;
import org.junit.Test;


public class ArraySortTest {

    @Test
    public void testSort() {
        ArraySort arraySort = new ArraySort(new Integer[]{123, 124, 125, 122, 121});
        arraySort.sort();
        Assert.assertArrayEquals(new Integer[]{121, 122, 123, 124, 125}, arraySort.getArray());
    }

    @Test
    public void testSortEqualElements() {
        ArraySort arraySort = new ArraySort(new Integer[]{111, 111, 111, 111, 111});
        arraySort.sort();
        Assert.assertArrayEquals(new Integer[]{111, 111, 111, 111, 111}, arraySort.getArray());
    }

    @Test
    public void testSortNull() {
        ArraySort arraySort = new ArraySort(null);
        arraySort.sort();
        Assert.assertArrayEquals(new Integer[0], arraySort.getArray());
    }
}
