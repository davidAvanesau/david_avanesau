package hw_9.hw_9_2;

import java.util.*;

public class MyHashSet<E> extends HashSet<E> {

    private HashSet<E> localHashSet;

    public MyHashSet() {
        localHashSet = new HashSet<>();
    }

    public MyHashSet(Collection<? extends E> c) {
        localHashSet = new HashSet<>(c);
    }

    public MyHashSet(int initialCapacity, float loadFactor) {
        localHashSet = new HashSet<>(initialCapacity, loadFactor);
    }

    public MyHashSet(int initialCapacity) {
        localHashSet = new HashSet<>(initialCapacity);
    }

    /**
     * this method return new HashSet with all elements of both sets
     *
     * @param hashSet
     * @return
     */
    public HashSet<E> union(HashSet<E> hashSet) {
        HashSet<E> currHashSet = new HashSet<>(localHashSet);
        currHashSet.addAll(hashSet);
        return currHashSet;
    }

    /**
     * this method return new HashSet with elements which are parts of both sets
     *
     * @param hashSet
     * @return
     */
    public HashSet<E> intersection(HashSet<E> hashSet) {
        HashSet<E> currHashSet = new HashSet<>();
        Iterator<E> localIterator = localHashSet.iterator();
        while (localIterator.hasNext()) {
            E localObj = localIterator.next();
            Iterator<E> iterator = hashSet.iterator();
            while (iterator.hasNext()) {
                E obj = iterator.next();
                if (obj.equals(localObj)) {
                    currHashSet.add(obj);
                }
            }
        }
        return currHashSet;
    }

    /**
     * this method return new HashSet with elements which are parts of first set
     * but not parts of second set
     *
     * @param hashSet
     * @return
     */
    public HashSet<E> minus(HashSet<E> hashSet) {
        HashSet<E> currHashSet = new HashSet<>(localHashSet);
        Iterator<E> localIterator = localHashSet.iterator();
        while (localIterator.hasNext()) {
            E localObj = localIterator.next();
            Iterator<E> iterator = hashSet.iterator();
            while (iterator.hasNext()) {
                E obj = iterator.next();
                if (obj.equals(localObj)) {
                    currHashSet.remove(obj);
                }
            }
        }
        return currHashSet;
    }

    /**
     * this method return new HashSet with elements which are not parts of both sets
     * but pats of each sets
     *
     * @param hashSet
     * @return
     */
    public HashSet<E> difference(HashSet<E> hashSet) {
        HashSet<E> currHashSet = new HashSet<>(localHashSet);
        currHashSet.addAll(hashSet);
        Iterator<E> localIterator = localHashSet.iterator();
        while (localIterator.hasNext()) {
            E localObj = localIterator.next();
            Iterator<E> iterator = hashSet.iterator();
            while (iterator.hasNext()) {
                E obj = iterator.next();
                if (obj.equals(localObj)) {
                    currHashSet.remove(obj);
                }
            }
        }
        return currHashSet;
    }

    @Override
    public boolean add(E e) {
        return localHashSet.add(e);
    }

    @Override
    public boolean remove(Object o) {
        return localHashSet.remove(o);
    }

    @Override
    public void clear() {
        localHashSet.clear();
    }

    @Override
    public Iterator<E> iterator() {
        return localHashSet.iterator();
    }

    @Override
    public int size() {
        return localHashSet.size();
    }

    @Override
    public boolean isEmpty() {
        return localHashSet.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return localHashSet.contains(o);
    }

    @Override
    public Object clone() {
        return localHashSet.clone();
    }

}
