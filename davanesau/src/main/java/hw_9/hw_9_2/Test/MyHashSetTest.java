package hw_9.hw_9_2.Test;

import hw_9.hw_9_2.MyHashSet;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;

public class MyHashSetTest {

    @Test
    public void testUnion() {
        MyHashSet<Integer> firstSet = new MyHashSet<>();
        firstSet.add(5);
        firstSet.add(10);
        HashSet<Integer> secondSet = new MyHashSet<>();
        secondSet.add(10);
        secondSet.add(15);
        HashSet<Integer> result;
        result = firstSet.union(secondSet);
        HashSet<Integer> expected = new HashSet<>();
        expected.add(5);
        expected.add(10);
        expected.add(15);
        Assert.assertEquals(result, expected);
    }

    @Test
    public void testDifference() {
        MyHashSet<Integer> firstSet = new MyHashSet<>();
        firstSet.add(5);
        firstSet.add(10);
        HashSet<Integer> secondSet = new MyHashSet<>();
        secondSet.add(10);
        secondSet.add(15);
        HashSet<Integer> result;
        result = firstSet.difference(secondSet);
        HashSet<Integer> expected = new HashSet<>();
        expected.add(5);
        expected.add(15);
        Assert.assertEquals(result, expected);
    }

    @Test
    public void testIntersection() {
        MyHashSet<Integer> firstSet = new MyHashSet<>();
        firstSet.add(5);
        firstSet.add(10);
        HashSet<Integer> secondSet = new MyHashSet<>();
        secondSet.add(10);
        secondSet.add(15);
        HashSet<Integer> result;
        result = firstSet.intersection(secondSet);
        HashSet<Integer> expected = new HashSet<>();
        expected.add(10);
        Assert.assertEquals(result, expected);
    }

    @Test
    public void testMinus() {
        MyHashSet<Integer> firstSet = new MyHashSet<>();
        firstSet.add(5);
        firstSet.add(10);
        HashSet<Integer> secondSet = new MyHashSet<>();
        secondSet.add(10);
        secondSet.add(15);
        HashSet<Integer> result;
        result = firstSet.minus(secondSet);
        HashSet<Integer> expected = new HashSet<>();
        expected.add(5);
        Assert.assertEquals(result, expected);
    }

    @Test
    public void testNull() {
        MyHashSet<Integer> firstSet = new MyHashSet<>();
        firstSet.add(5);
        firstSet.add(10);
        HashSet<Integer> secondSet = new MyHashSet<>();
        HashSet<Integer> result;
        result = firstSet.union(secondSet);
        HashSet<Integer> expected = new HashSet<>();
        expected.add(5);
        expected.add(10);
        Assert.assertEquals(result, expected);
    }


}
