package hw_14;

/**
 * An entry point to the program.
 */
public class Main {

	/**
	 * An entry point to the program.
	 *
	 * @param args it's program arguments.
	 */
	public static void main(String[] args) {

		System.out.println(Controller.control(args));
	}
}
