package hw_14.exception;

public class BadConnectionException extends RuntimeException {

	public BadConnectionException(String message) {
		super(message);
	}
}
