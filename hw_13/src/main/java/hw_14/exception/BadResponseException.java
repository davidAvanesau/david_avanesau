package hw_14.exception;

public class BadResponseException extends RuntimeException {

	public BadResponseException(String message) {
		super(message);
	}
}
