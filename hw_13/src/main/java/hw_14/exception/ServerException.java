package hw_14.exception;

public class ServerException extends RuntimeException {

	public ServerException(String message) {
		super(message);
	}
}
