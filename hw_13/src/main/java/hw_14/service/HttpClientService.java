package hw_14.service;

import hw_14.Article;
import hw_14.exception.BadConnectionException;
import hw_14.exception.ServerException;
import hw_14.util.Parser;
import hw_14.util.ResponseReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * Service realization that uses apache.http functionality.
 */
public class HttpClientService implements Service {

	Logger logger = Logger.getLogger(HttpClientService.class.getName());
	/**
	 * Uses GET HTTP method returns string representation of article with id.
	 *
	 * @param id it's article id.
	 * @return string representation of article with id.
	 */
	@Override
	public String getArticle(int id) {
		logger.info("get article...");
		String responseMessage;
		CloseableHttpClient client = HttpClientBuilder.create().build();
		HttpGet get = new HttpGet(URL + "/" + id);
		try {
			HttpResponse response = client.execute(get);
			responseMessage = ResponseReader.getResponse(response);
		} catch (IOException e) {
			e.printStackTrace();
			logger.info("IOException");
			responseMessage = null;
		}
		if (responseMessage == null) {
			throw new BadConnectionException("Connection failed");
		}
		return responseMessage;
	}

	/**
	 * Puts article uses POST HTTP method.
	 *
	 * @param article it's article to put.
	 * @return response message.
	 */
	@Override
	public String putArticle(Article article) {
		logger.info("put article...");
		String output;
		CloseableHttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(URL);
		post.setHeader("Content-Type", "application/json; charset=UTF-8");
		post.setEntity(new StringEntity(Parser.toJson(article), "UTF-8"));
		HttpResponse response;
		try {
			response = client.execute(post);
			if (response.getStatusLine().getStatusCode() != 201) {
				throw new ServerException(response.getStatusLine().toString() + "\n" + ResponseReader.getResponse(response));
			}
			output = Parser.toJson(article);
		} catch (IOException e) {
			e.printStackTrace();
			logger.info("IOException");
			output = null;
		}
		if (output == null) {
			throw new BadConnectionException("Connection failed");
		}
		return output;
	}
}
