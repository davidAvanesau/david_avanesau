package hw_14.service;


import hw_14.Article;
import hw_14.exception.BadConnectionException;
import hw_14.exception.BadResponseException;
import hw_14.util.Parser;
import hw_14.util.ResponseReader;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Logger;

/**
 * Service realization that uses java.net functionality.
 */
public class JavaNetService implements Service {

	Logger logger = Logger.getLogger(JavaNetService.class.getName());
	/**
	 * Uses GET HTTP method returns string representation of article with id.
	 *
	 * @param id it's article id.
	 * @return string representation of article with id.
	 */
	@Override
	public String getArticle(int id) {
		logger.info("get article...");
		URLConnection connection;
		String responseMessage;
		try {
			connection = new URL(URL + "/" + id).openConnection();
			responseMessage = ResponseReader.getResponse(connection);
		} catch (IOException e) {
			e.printStackTrace();
			logger.info("IOException");
			responseMessage = null;
		}
		if (responseMessage == null) {
			throw new BadConnectionException("Connection failed");
		}
		return responseMessage;
	}

	/**
	 * Puts article uses POST HTTP method.
	 *
	 * @param article it's article to put.
	 * @return response message.
	 */
	@Override
	public String putArticle(Article article) {
		logger.info("put article...");
		String output;
		HttpURLConnection connection = null;
		OutputStream os = null;
		try {
			URL url = new URL(URL);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			connection.setDoOutput(true);
			os = connection.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
			osw.write(Parser.toJson(article));
			osw.flush();
			osw.close();
			if (connection.getResponseCode() == HttpURLConnection.HTTP_CREATED) {
				output = Parser.toJson(article);
			} else {
				throw new BadResponseException(ResponseReader.getResponse(connection));
			}
		} catch (IOException e) {
			e.printStackTrace();
			logger.info("IOException");
			output = null;
		} finally {
			try {
				if (os != null) {
					os.close();
				}
				if (connection != null) {
					connection.disconnect();
				}
			} catch (IOException e) {
				e.printStackTrace();
				logger.info("problem with closing the connection");
			}
		}
		if (output == null) {
			throw new BadConnectionException("Connection failed");
		}
		return output;
	}
}
