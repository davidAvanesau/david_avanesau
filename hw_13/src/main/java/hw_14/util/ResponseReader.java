package hw_14.util;

import hw_14.exception.BadResponseException;
import hw_14.exception.ServerException;
import org.apache.http.HttpResponse;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.Scanner;

/**
 * Util class for getting https responses.
 */
public class ResponseReader {

	private ResponseReader() {
	}

	/**
	 * Returns response message from HttpResponse.
	 *
	 * @param response it's HttpResponse.
	 * @return response message.
	 */
	public static String getResponse(HttpResponse response) {
		String responseMessage;
		try (InputStream stream = response.getEntity().getContent()) {
			responseMessage = read(stream);
		} catch (IOException e) {
			e.printStackTrace();
			responseMessage = null;
		}
		if (responseMessage == null) {
			throw new BadResponseException("Response reading failed");
		}
		if (response.getStatusLine().getStatusCode() == 404) {
			throw new ServerException("FileNotFound 404");
		}
		return responseMessage;
	}

	/**
	 * Returns response message from URLConnection.
	 *
	 * @param connection it's URLConnection.
	 * @return response message.
	 */
	public static String getResponse(URLConnection connection) {
		String responseMessage;
		try (InputStream stream = connection.getInputStream()) {
			responseMessage = read(stream);
		} catch (IOException e) {
			e.printStackTrace();
			responseMessage = null;
		}
		if (responseMessage == null) {
			throw new BadResponseException("Response reading failed");
		}
		return responseMessage;
	}

	/**
	 * Read  response message from response stream.
	 *
	 * @param responseStream response input stream.
	 * @return response message.
	 */
	private static String read(InputStream responseStream) {
		Scanner scanner = new Scanner(responseStream);
		StringBuilder responseMessage = new StringBuilder();
		while (scanner.hasNextLine()) {
			responseMessage.append(scanner.nextLine());
		}
		return responseMessage.toString();
	}
}
