package hw_14;

import hw_14.exception.ValidationException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ControllerTest {

	private String expectedGet = "Article [1]: User [1] Title [ \"sunt aut facere repellat provident occaecati excepturi optio reprehenderit\"] Message [\"quia et suscipit\n" +
			"suscipit recusandae consequuntur expedita et cum\n" +
			"reprehenderit molestiae ut ut quas totam\n" +
			"nostrum rerum est autem sunt rem eveniet architecto\"]";

	private String expectedPost = "Article [101] has been created: User [1] Title [ \"some title\"] Message [\"some body\"]";

	@Test
	public void testControllerGetFirst() {
		String actual = Controller.control(new String[]{"GET", "1", "1"});
		assertEquals(expectedGet, actual);
	}

	@Test
	public void testControllerGetSecond() {
		String actual = Controller.control(new String[]{"GET", "1", "2"});
		assertEquals(expectedGet, actual);
	}

	@Test
	public void testControllerPostFirst() {
		String actual = Controller.control(new String[]{"POST", "101", "1"});
		assertEquals(expectedPost, actual);
	}

	@Test
	public void testControllerPostSecond() {
		String actual = Controller.control(new String[]{"POST", "101", "2"});
		assertEquals(expectedPost, actual);
	}

	@Test(expected = ValidationException.class)
	public void testControllerWithValidationException() {
		Controller.control(new String[]{"POST", "101", "3"});
	}

	@Test(expected = ValidationException.class)
	public void testControllerWithIncorrectMethod() {
		Controller.control(new String[]{"PUT", "101", "1"});
	}

}