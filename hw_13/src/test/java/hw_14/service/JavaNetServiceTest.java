package hw_14.service;

import hw_14.Article;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class JavaNetServiceTest {

	JavaNetService service;

	@Before
	public void setUp() {
		service = mock(JavaNetService.class);
	}


	@Test
	public void testJavaNetServiceGet(){
		when(service.getArticle(anyInt())).thenReturn("{\"id\":101,\"title\":\"Any title\",\"body\":\"Any body\",\"userId\":1}");
		Article expected = new Article(1, 101, "Any title", "Any body");
		String actual = service.getArticle(1);
		Assert.assertEquals(new JSONObject(expected).toString(), actual);
	}

	@Test
	public void testJavaNetServicePost(){
		when(service.putArticle(any(Article.class))).thenReturn("{\"id\":101,\"title\":\"Any title\",\"body\":\"Any body\",\"userId\":1}");
		Article expected = new Article(1, 101, "Any title", "Any body");
		String actual = service.putArticle(new Article(1, 101, "Any title", "Any body"));
		Assert.assertEquals(new JSONObject(expected).toString(), actual);
	}

//	@Test
//	public void testGetArticle() {
//
//		String json = new JavaNetService().getArticle(1);
//		String sourceJson = "{\n" +
//				"    \"userId\": 1,\n" +
//				"    \"id\": 1,\n" +
//				"    \"title\": \"sunt aut facere repellat provident occaecati excepturi optio reprehenderit\",\n" +
//				"    \"body\": \"quia et suscipit\\nsuscipit recusandae consequuntur expedita et cum\\nreprehenderit molestiae ut ut quas totam\\nnostrum rerum est autem sunt rem eveniet architecto\"\n" +
//				"  }";
//		assertEquals(new JSONObject(sourceJson).toString(), new JSONObject(json).toString());
//	}
//
//	@Test(expected = BadResponseException.class)
//	public void testGetArticleWithException() {
//		String json = new JavaNetService().getArticle(101);
//	}
//
//	@Test
//	public void testPutArticle() {
//		Article article = new Article(1, 101, "some title", "some body");
//		String response = new JavaNetService().putArticle(article);
//		assertEquals(new JSONObject(article).toString(), new JSONObject(response).toString());
//	}

}