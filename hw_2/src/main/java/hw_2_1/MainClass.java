package hw_2_1;

public class MainClass {
    /**
     * The main method of class that calculate and output g if
     *
     * @param args is valid
     */
    public static void main(String[] args) {
        if (args.length == 4) {
            int a = Integer.parseInt(args[0]);
            int p = Integer.parseInt(args[1]);
            double m1 = Double.parseDouble(args[2]);
            double m2 = Double.parseDouble(args[3]);
            double numerator = 4 * Math.pow(Math.PI, 2) * Math.pow(a, 3);
            double denominator = Math.pow(p, 2) * (m1 + m2);
            if (isDenominatorNotNull(denominator)) {
                double g = numerator / denominator;
                System.out.println(g);
            } else {
                System.out.println("Denominator is null!");
            }
        } else {
            System.out.println("Incorrect data");
        }
    }

    /**
     * Checks @param denominator value
     *
     * @return true if it isn't null
     */
    private static boolean isDenominatorNotNull(double denominator) {
        if (denominator == 0) {
            return false;
        } else {
            return true;
        }
    }
}
