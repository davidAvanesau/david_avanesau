package hw_2_1;

public class MainClassTest {

    private static String[] args = new String[4];

    public static void main(String[] args) {
        testMain29Return();
        System.out.println();
        testMain19Return();
        System.out.println();
        testMainDenominatorIsNull();
        System.out.println();
        testMainIncorrectData();
    }

    private static void testMain29Return() {
        args[0] = "3";
        args[1] = "2";
        args[2] = "3.5";
        args[3] = "5.6";
        System.out.println("Right output is:");
        System.out.println("29.283441629605786");
        System.out.println("Method output is:");
        MainClass.main(args);
    }

    private static void testMain19Return() {
        args[0] = "1";
        args[1] = "1";
        args[2] = "1";
        args[3] = "1";
        System.out.println("Right output is:");
        System.out.println("19.739208802178716");
        System.out.println("Method output is:");
        MainClass.main(args);
    }

    private static void testMainDenominatorIsNull() {
        args[0] = "1";
        args[1] = "1";
        args[2] = "0";
        args[3] = "0";
        System.out.println("Right output is:");
        System.out.println("Denominator is null!");
        System.out.println("Method output is:");
        MainClass.main(args);
    }

    private static void testMainIncorrectData() {
        String[] args = new String[1];
        args[0] = "1";
        System.out.println("Right output is:");
        System.out.println("Incorrect data");
        System.out.println("Method output is:");
        MainClass.main(args);
    }

}
