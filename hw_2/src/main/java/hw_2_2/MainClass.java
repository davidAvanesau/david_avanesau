package hw_2_2;


public class MainClass {

    /**
     * Main method that calls one of six methods base on first and second values of
     *
     * @param args
     */
    public static void main(String[] args) {
        final int algorithmId = Integer.parseInt(args[0]);
        final int loopType = Integer.parseInt(args[1]);
        final int n = Integer.parseInt(args[2]);
        if (n >= 1) {
            if (algorithmId == 1) {
                if (loopType == 1) {
                    System.out.println(fibonacciWhile(n));
                } else if (loopType == 2) {
                    System.out.println(fibonacciDoWhile(n));
                } else if (loopType == 3) {
                    System.out.println(fibonacciFor(n));
                } else {
                    System.out.println("Incorrect Loop type!");
                }
            } else if (algorithmId == 2) {
                if (loopType == 1) {
                    System.out.println(factorialWhile(n));
                } else if (loopType == 2) {
                    System.out.println(factorialDoWhile(n));
                } else if (loopType == 3) {
                    System.out.println(factorialFor(n));
                } else {
                    System.out.println("Incorrect Loop type!");
                }
            } else {
                System.out.println("Incorrect Algorithm Id!");
            }
        } else {
            System.out.println("Incorrect n!");
        }
    }

    /**
     * @param n is the number of values ​​in the fibonacci series
     * @return this series
     * This method use while loop
     */
    private static String fibonacciWhile(int n) {
        if (n == 1) {
            return "0";
        } else {
            int[] fibonacci = new int[n];
            fibonacci[0] = 0;
            fibonacci[1] = 1;
            String result = "0 1";
            int i = 2;
            while (n > 2) {
                fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
                result += " " + fibonacci[i];
                i++;
                n--;
            }
            return result;
        }
    }

    /**
     * @param n is the number of values ​​in the fibonacci series
     * @return this series
     * This method use do-while loop
     */
    private static String fibonacciDoWhile(int n) {
        if (n == 1) {
            return "0";
        } else {
            int[] fibonacci = new int[n];
            fibonacci[0] = 0;
            fibonacci[1] = 1;
            String result = "0 1";
            int i = 2;
            do {
                if (n > 2) {
                    fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
                    result += " " + fibonacci[i];
                    i++;
                    n--;
                }
            } while (n > 2);
            return result;
        }
    }

    /**
     * @param n is the number of values ​​in the fibonacci series
     * @return this series
     * This method use for loop
     */
    private static String fibonacciFor(int n) {
        if (n == 1) {
            return "0";
        } else if (n == 2) {
            return "0 1";
        } else {
            int[] fibonacci = new int[n];
            fibonacci[0] = 0;
            fibonacci[1] = 1;
            String result = "0 1";
            for (int i = 2; i < n; i++) {
                fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
                result += " " + fibonacci[i];
            }
            return result;
        }
    }

    /**
     * In this method factorial will be find for
     *
     * @param n
     * @return factorial
     * This method use for loop
     */
    private static long factorialFor(int n) {
        long factorial = 1;
        for (int i = 2; i <= n; i++) {
            factorial *= i;
        }
        return factorial;
    }

    /**
     * In this method factorial will be find for
     *
     * @param n
     * @return factorial
     * This method use while loop
     */
    private static long factorialWhile(int n) {
        long factorial = 1;
        int i = 1;
        while (i <= n) {
            factorial *= i;
            i++;
        }
        return factorial;
    }

    /**
     * In this method factorial will be find for
     *
     * @param n
     * @return factorial
     * This method use do-while loop
     */
    private static long factorialDoWhile(int n) {
        long factorial = 1;
        int i = 1;
        do {
            factorial *= i;
            i++;
        } while (i <= n);
        return factorial;
    }
}

