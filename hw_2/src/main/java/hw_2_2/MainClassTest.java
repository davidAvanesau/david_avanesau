package hw_2_2;

public class MainClassTest {

    static String[] args = new String[3];

    public static void main(String[] args) {
        testFibonacciWhile();
        System.out.println();
        testFibonacciDoWhile();
        System.out.println();
        testFibonacciFor();
        System.out.println();
        testFacrorialWhile();
        System.out.println();
        testFactorialDoWhile();
        System.out.println();
        testFactorialFor();
        System.out.println();
        testIncorrectAlgorithmId();
        System.out.println();
        testIncorrectLoopType();
        System.out.println();
        testIncorrectN();


    }

    private static void testFibonacciWhile() {
        args[0] = "1";
        args[1] = "1";
        args[2] = "5";
        System.out.println("Right output is:");
        System.out.println("0 1 1 2 3");
        System.out.println("Method output is:");
        MainClass.main(args);
    }

    private static void testFibonacciDoWhile() {
        args[0] = "1";
        args[1] = "2";
        args[2] = "5";
        System.out.println("Right output is:");
        System.out.println("0 1 1 2 3");
        System.out.println("Method output is:");
        MainClass.main(args);
    }

    private static void testFibonacciFor() {
        args[0] = "1";
        args[1] = "3";
        args[2] = "5";
        System.out.println("Right output is:");
        System.out.println("0 1 1 2 3");
        System.out.println("Method output is:");
        MainClass.main(args);
    }

    private static void testFacrorialWhile() {
        args[0] = "2";
        args[1] = "1";
        args[2] = "5";
        System.out.println("Right output is:");
        System.out.println("120");
        System.out.println("Method output is:");
        MainClass.main(args);
    }

    private static void testFactorialDoWhile() {
        args[0] = "2";
        args[1] = "2";
        args[2] = "5";
        System.out.println("Right output is:");
        System.out.println("120");
        System.out.println("Method output is:");
        MainClass.main(args);
    }

    private static void testFactorialFor() {
        args[0] = "2";
        args[1] = "3";
        args[2] = "5";
        System.out.println("Right output is:");
        System.out.println("120");
        System.out.println("Method output is:");
        MainClass.main(args);
    }

    private static void testIncorrectAlgorithmId() {
        args[0] = "0";
        args[1] = "1";
        args[2] = "5";
        System.out.println("Right output is:");
        System.out.println("Incorrect Algorithm Id!");
        System.out.println("Method output is:");
        MainClass.main(args);
    }

    private static void testIncorrectLoopType() {
        args[0] = "1";
        args[1] = "0";
        args[2] = "5";
        System.out.println("Right output is:");
        System.out.println("Incorrect Loop type!");
        System.out.println("Method output is:");
        MainClass.main(args);
    }

    private static void testIncorrectN() {
        args[0] = "2";
        args[1] = "3";
        args[2] = "0";
        System.out.println("Right output is:");
        System.out.println("Incorrect n!");
        System.out.println("Method output is:");
        MainClass.main(args);
    }
}
